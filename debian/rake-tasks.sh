#! /bin/sh

set -e

# Read debian specific configuration
. /etc/gitlab/gitlab-debian.conf
export DB RAILS_ENV

cd /usr/share/gitlab

# Check if the db is already present
db_relations="$(LANG=C runuser -u postgres -- sh -c "psql gitlab_production -c \"\d\"" 2>&1)"
if [ "$db_relations" = "No relations found." ] || \
  [ "$db_relations" = "Did not find any relations." ]; then
  echo "Initializing database..."
  test -f ${gitlab_home}/db/structure.sql || \
  runuser -u ${gitlab_user} -- sh -c \
  "cp ${gitlab_data_dir}/db/structure.sql.template ${gitlab_data_dir}/db/structure.sql"
  runuser -u ${gitlab_user} -- sh -c \
  "touch ${gitlab_data_dir}/.gitlab_shell_secret"
  runuser -u ${gitlab_user} -- sh -c 'touch /var/lib/gitlab/secrets.yml'
  runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake db:structure:load'
else
  echo "gitlab_production database is not empty, skipping gitlab setup"
fi

runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake db:migrate'

# Restrict permissions for secret files
chmod 0700 ${gitlab_data_dir}/.gitlab_shell_secret

echo "Installing node modules..."
runuser -u ${gitlab_user} -- sh -c 'install -d /var/lib/gitlab/.node_modules'
runuser -u ${gitlab_user} -- sh -c 'install -d /var/lib/gitlab/.cache'
runuser -u ${gitlab_user} -- sh -c 'yarnpkg install'
# Remove write permissions of .yarn-metadata.json files
runuser -u ${gitlab_user} -- sh -c 'test -d "/var/lib/gitlab/.cache/yarn/v6" && find /var/lib/gitlab/.cache/yarn/v6/ -name .yarn-metadata.json -perm -a=w -exec chmod 644 {} \;'

echo "Precompiling locales..."
runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake gettext:po_to_json'

echo "Precompiling assets..."
runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake tmp:cache:clear assets:precompile'

echo "Webpacking..."
# Workaround for webpack crashing with nodejs 10 - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956211
# Build assets in production mode - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956508
runuser -u ${gitlab_user} -- sh -c 'NODE_ENV="production" NODE_OPTIONS="--max-old-space-size=4096" webpack --config config/webpack.config.js'
